{
  "$schema": "https://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#",
  "contentVersion": "1.0.0.0",
  "parameters": {

    "vNETName": {
      "defaultValue": "VNET",
      "metadata": {
        "description": "Virtual Network Name"
      },
      "type": "string"
    },

    "vNETPrefix": {
      "defaultValue": "10.1.0.0/16",
      "metadata": {
        "description": "Virtual Network Address Space"
      },
      "type": "string"
    },

    "vNETSubIntName": {
      "defaultValue": "LAN",
      "metadata": {
        "description": "Internal subnet "
      },
      "type": "string"
    },

    "vNETSubExtName": {
      "defaultValue": "DMZ",
      "metadata": {
        "description": "External subnet "
      },
      "type": "string"
    },



    "vNETSubGtwName": {
      "defaultValue": "GatewaySubnet",
      "metadata": {
        "description": "Gateway subnet "
      },
      "type": "string"
    },

    "vNETSubIntPrefix": {
      "defaultValue": "10.1.0.0/24",
      "metadata": {
        "description": "Internal IP subnet"
      },
      "type": "string"
    },

    "vNETSubExtPrefix": {
      "defaultValue": "10.1.1.0/24",
      "metadata": {
        "description": "External IP subnet"
      },
      "type": "string"
    },



    "vNETSubGtwPrefix": {
      "defaultValue": "10.1.255.0/24",
      "metadata": {
        "description": "Gateway IP subnet"
      },
      "type": "string"
    },

    "VPNIP": {
      "defaultValue": "VPNIP",
      "metadata": {
        "description": "Public IP name"
      },
      "type": "string"
    },

  

    "VPNOnPremWAN": {
      "defaultValue": "x.x.x.x",
      "metadata": {
        "description": "On Prem WAN"
      },
      "type": "string"
    },

    "S2SGtwAzureName": {
      "defaultValue": "S2S-Azure-GW",
      "metadata": {
        "description": "Azure Gateway name"
      },
      "type": "string"
    },

    "S2SGtwVPNType": {
      "allowedValues": [
        "RouteBased",
        "PolicyBased"
      ],
      "defaultValue": "RouteBased",
      "metadata": {
        "description": "Route based or policy based"
      },
      "type": "string"
    },

    "S2SLocalIPSubnet": {
      "defaultValue": "10.1.0.0/24",
      "metadata": {
        "description": "On-Premise IP subnet"
      },
      "type": "string"
    },
    "StoAcctLogName": {
      "defaultValue": "logstore",
      "metadata": {
        "description": "Logs Storage account name"
      },
      "type": "string"
    },

    "StoAcctLogType": {
      "allowedValues": [
        "Standard_LRS",
        "Standard_GRS",
        "Standard_ZRS"
      ],
      "defaultValue": "Standard_LRS",
      "metadata": {
        "description": "Log Storage account type"
      },
      "type": "string"
    }

  },
  "variables": {
    "API-Version": "2016-03-30",
    "ResourcesLocation": "[ResourceGroup().location]",
    "vNETID": "[resourceId('Microsoft.Network/virtualNetworks', parameters('vNETName'))]",
    "vNETSubGtwRef": "[concat(variables('vNETID'), '/subnets/', parameters('vNETSubGtwName'))]",
    "vNETSubExtRef": "[concat(variables('vNETID'), '/subnets/', parameters('vNETSubExtName'))]",
    "vNETSubIntRef": "[concat(variables('vNETID'), '/subnets/', parameters('vNETSubIntName'))]",
    "vNETSubCluRef": "[concat(variables('vNETID'), '/subnets/', parameters('vNETSubCluName'))]"

  },
  "resources": [
    {
      "apiVersion": "[variables('API-Version')]",
      "location": "[variables('ResourcesLocation')]",
      "name": "[parameters('vNETName')]",
      "properties": {
        "addressSpace": {
          "addressPrefixes": [
            "[parameters('vNETPrefix')]"
          ]
        },
        "subnets": [
          {
            "name": "[parameters('vNETSubIntName')]",
            "properties": {
              "addressPrefix": "[parameters('vNETSubIntPrefix')]"
            }
          },
          {
            "name": "[parameters('vNETSubExtName')]",
            "properties": {
              "addressPrefix": "[parameters('vNETSubExtPrefix')]"
            }
          },
          {
            "name": "[parameters('vNETSubCluName')]",
            "properties": {
              "addressPrefix": "[parameters('vNETSubCluPrefix')]"
            }
          },
          {
            "name": "[Parameters('vNETSubGtwName')]",
            "properties": {
              "addressPrefix": "[Parameters('vNETSubGtwPrefix')]"
            }
          }
        ]
      },
      "tags": {
        "displayName": "Virtual Network"
      },
      "type": "Microsoft.Network/virtualNetworks"
    },
    {
      "apiVersion": "[variables('API-Version')]",
      "location": "[variables('ResourcesLocation')]",
      "name": "[parameters('VPNIP')]",
      "properties": {
        "publicIPAllocationMethod": "Dynamic"
      },
      "tags": {
        "displayName": "Public IP Address"
      },
      "type": "Microsoft.Network/publicIPAddresses"
    },
    {
      "apiVersion": "[variables('API-version')]",
      "location": "[variables('ResourcesLocation')]",
      "name": "[parameters('S2SGtwOnPremName')]",
      "properties": {
        "localNetworkAddressSpace": {
          "addressPrefixes": [
            "[parameters('S2SLocalIPSubnet')]"
          ]
        },
        "gatewayIpAddress": "[parameters('VPNOnPremWAN')]"
      },
      "tags": {
        "displayName": "Local Gateway"
      },
      "type": "Microsoft.Network/localNetworkGateways"
    },
    {
      "apiVersion": "[variables('API-version')]",
      "dependsOn": [
        "[concat('Microsoft.Network/publicIPAddresses/', parameters('VPNIP'))]",
        "[concat('Microsoft.Network/virtualNetworks/', parameters('vNETName'))]"
      ],
      "location": "[Variables('Resourceslocation')]",
      "name": "[parameters('S2SGtwAzureName')]",
      "properties": {
        "enableBgp": false,
        "gatewayType": "Vpn",
        "ipConfigurations": [
          {
            "properties": {
              "privateIPAllocationMethod": "Dynamic",
              "publicIPAddress": {
                "id": "[resourceId('Microsoft.Network/publicIPAddresses',parameters('VPNIP'))]"
              },
              "subnet": {
                "id": "[variables('vNETSubGtwRef')]"
              }
            },
            "name": "vnetGatewayConfig"
          }
        ],
        "vpnType": "[parameters('S2SGtwVPNType')]"
      },
      "tags": {
        "displayName": "Azure Gateway"
      },
      "type": "Microsoft.Network/virtualNetworkGateways"
    },

    {
      "name": "[parameters('StoAcctLogName')]",
      "type": "Microsoft.Storage/storageAccounts",
      "apiVersion": "2016-05-01",
      "tags": {
        "displayName": "Log Storage Account"
      },
      "sku": {
        "name": "[parameters('StoAcctLogType')]"
      },
      "kind": "Storage",
      "location": "[variables('ResourcesLocation')]"
    }


  ],
  "outputs": {}
}